import Sudoku_solver as sudoku

container = sudoku.container
container1 = []
container1.append([5,3,0,0,7,0,0,0,0])
container1.append([6,0,0,1,9,5,0,0,0])
container1.append([0,9,8,0,0,0,0,6,0])
container1.append([8,0,0,0,6,0,0,0,3])
container1.append([4,0,0,8,0,3,0,0,1])
container1.append([7,0,0,0,2,0,0,0,6])
container1.append([0,6,0,0,0,0,2,8,0])
container1.append([0,0,0,4,1,9,0,0,5])
container1.append([0,0,0,0,8,0,0,7,9])

def draw_container(container):
    list_number = [0,1,2,3,4,5,6,7,8]
    print("//////////////////////")
    print()
    print("  ", end = " ")
    for i in list_number:
        if i % 3 == 0 and i <8 and i>0:
            print(" ", end = " ")
        print(i, end = " ")
    print("\n" + "  "+"______________________")
    for x, row in enumerate(container):
        print(x, end = "| ")
        for y, val in enumerate(row):
            if y % 3 == 0 and y <8 and y>0:
                print("|", end = " ")
            print(val, end = " ")
        print()
        if (x-2) % 3 == 0:
            print("  "+"______________________")
            print()
    print("//////////////////////")
    print()

def player_input(container, container1):
    try:
        x,y,val = input("Row:? Column:? Value:? ").split(",")
        x= int(x)
        y = int(y)
        val = int(val)
        if x == 10 and y == 10 and val == 10:
            zero = -1
            sudoku.solving(container1)
        else:
            container[x][y] = val
            draw_container(container)
            zero = sudoku.check_zero(0,container)
        return container, zero  

    except:
        print("You're supposed to input 3 integers separated by a comma")
        player_input(container,container1) 

def check_whole(container):
    while True:
        set_subtract = {1,2,3,4,5,6,7,8,9}

        # Check row:
        row_val = []
        for row in container:
            for i in row:
                row_val.append(i)
            if len(set_subtract - set(row_val)) != 0:
                print("You have made some mistakes in row number",row)
                break
            row_val.clear()

        # Check column:
        row_val = []
        for y in range(9):
            for x in range(9):
                row_val.append(container[x][y])
            if len(set_subtract - set(row_val)) != 0:
                print("You have made some mistakes in column number",y)
                break
            row_val.clear()
                
        #Check square:
        square_val = []
        first = [0,1,2]
        second = [3,4,5]
        third = [6,7,8]
        set_group = [first,second,third]
        for i in set_group:
            for j in set_group:
                for x in range(3):
                    for y in range(3):
                        square_val.append(container[i[x]][j[y]])
                if len(set_subtract - set(square_val)) != 0:
                    print("You have made some mistakes in square(x,y)")
                    break
                square_val.clear()          
        print("Congrats! You've done the sudoku correctly.")
        break
if __name__ == "__main__":
    draw_container(container)
    print(
        """
        Welcome to the Sudoku World
        Input your choice following the order: row - column - value
        When you get stuck and need to see the answers, input: 10,10,10
        The last point to follow: Enjoy the game ^.^ 
        """
    )
    zero = sudoku.check_zero(0,container)
    while zero > 0:
        container, zero = player_input(container,container1)
    if zero == 0:
        choice = input("Do you want to check your answers? - (Yes or No) ")
        while choice.capitalize() == "No":
            draw_container(container)
            container, zero = player_input(container,container1)
            choice = input("Do you want to check your answers? - (Yes or No) ")
        check_whole(container)

            

    
    
