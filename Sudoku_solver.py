import numpy as np
import time

#Set up Sudoku
container = []
container.append([5,3,0,0,7,0,0,0,0])
container.append([6,0,0,1,9,5,0,0,0])
container.append([0,9,8,0,0,0,0,6,0])
container.append([8,0,0,0,6,0,0,0,3])
container.append([4,0,0,8,0,3,0,0,1])
container.append([7,0,0,0,2,0,0,0,6])
container.append([0,6,0,0,0,0,2,8,0])
container.append([0,0,0,4,1,9,0,0,5])
container.append([0,0,0,0,8,0,0,7,9])

def print_container(container):
    print("//////////////////////")
    print()
    for x, row in enumerate(container):
        for y, val in enumerate(row):
            if y % 3 == 0 and y <8 and y>0:
                print("|", end = " ")
            print(val, end = " ")
        print()
        if (x-2) % 3 == 0:
            print("______________________")
            print()
    print("//////////////////////")
    print()

# x is the number of the row
# y is the number of the column

def check_horizontal(x,y,container):
    set_substract = {1,2,3,4,5,6,7,8,9}
    return set_substract - set(container[x])

def check_vertical(x,y,container):
    set_substract = {1,2,3,4,5,6,7,8,9}
    col_poss = []
    for i in range(9):
        col_poss.append(container[i][y])
    return set_substract - set(col_poss)

def check_square(x,y,container):
    set_substract = {1,2,3,4,5,6,7,8,9}
    first = [0,1,2]
    second = [3,4,5]
    third = [6,7,8]
    set_square = [first, second, third]
    for m in set_square:
        if x in m:
            group_row = m
        if y in m:
            group_col = m
    ret_set = []
    for i in group_row:
        for j in group_col:
            ret_set.append(container[i][j])
    return set_substract - set(ret_set)

def poss_value(x,y,container):
    poss_value = list(check_square(x,y,container).intersection(check_horizontal(x,y,container)).intersection(check_vertical(x,y,container)))
    return poss_value

def explicit_check(container):
    stump_count = 1
    for x in range(9):
        for y in range(9):
            if container[x][y] == 0:
                poss_values = poss_value(x,y,container)
                if len(poss_values) == 1:
                    container[x][y] = list(poss_values)[0]
                    stump_count = 0
                    print_container(container)
    return container, stump_count 

def implicit_check(x,y,container):
    if container[x][y] == 0:
        poss_values = poss_value(x,y,container)

        # Check row:
        row_poss = []
        for j in range(9):
            if j == y:
                continue
            if container[x][j] == 0:
                for val in poss_value(x,j,container):
                    row_poss.append(val)
        if len(set(poss_values) - set(row_poss)) == 1:
            container[x][y] = list(set(poss_values)-set(row_poss))[0]
            print_container(container)

        # Check column:
        col_poss = []
        for i in range(9):
            if i == x:
                continue
            if container[i][y] == 0:
                for val in poss_value(i,y,container):
                    col_poss.append(val)
        if len(set(poss_values)-set(col_poss)) == 1:
            container[i][y] = list(set(poss_values)-set(col_poss))[0]
            print_container(container)

        # Check square:
        square_poss = []
        first = [0,1,2]
        second = [3,4,5]
        third = [6,7,8]
        set_group = [first,second,third]
        for m in set_group:
            if x in m:
                row_group = m
            if y in m:
                col_group = m
        for i in row_group:
            for j in col_group:
                if container[i][j] == 0:
                    for val in poss_value(i,j,container):
                        square_poss.append(val)
        if len(set(poss_values) - set(square_poss)) == 1:
            container[x][y] = list(set(poss_values)-set(square_poss))[0]
            print_container(container)
    return container

def check_zero(zero, container):
    for i in range(9):
        for j in range(9):
            if container[i][j] == 0:
                zero += 1
    return zero

def solving(container):
    start = time.time()
    move = check_zero(0,container)
    print_container(container)
    print(f"We have to make {move} moves.")
    solve = True
    while solve:
        container, stump_count = explicit_check(container)

        if check_zero(0, container) == 0:
            time_to_solve = time.time() - start
            print(f"It took {time_to_solve} seconds to solve the sudoku.")
            solve = False

        if stump_count > 0:
            for x in range(9):
                for y in range(9):
                    container = implicit_check(x,y,container)
    print()

if __name__ == "__main__":
    solving(container)
